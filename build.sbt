val ORGANIZATION = "jp.akzero"
val VERSION = "0.1.0"
val SCALA_VERSION = "2.11.8"
val DEFAULT_SETTINGS = Seq(
  organization := ORGANIZATION,
  version := VERSION,
  licenses += ("Apache-2.0", url("http://opensource.org/licenses/Apache-2.0")),
  scalaVersion := SCALA_VERSION
)
val ROOT_ID = "scalacss-bootstrap-root"
val SCALACSS_BOOTSTRAP_ID = "scalacss-bootstrap"

lazy val root = Project(id = ROOT_ID, base = file("."))
  .settings(DEFAULT_SETTINGS)
  .settings(
    name := ROOT_ID
  )
  .aggregate(scalacssBootstrap)

lazy val scalacssBootstrap = Project(id = SCALACSS_BOOTSTRAP_ID, base = file(SCALACSS_BOOTSTRAP_ID))
  .settings(DEFAULT_SETTINGS)
  .settings(
    name := SCALACSS_BOOTSTRAP_ID,
    libraryDependencies ++= Seq(
      "com.github.japgolly.scalacss" %%% "core" % "0.4.1",
      "com.github.japgolly.scalacss" %%% "ext-react" % "0.4.1"
    ),
    persistLauncher := true,
    mainClass in Compile := Some("jp.akzero.pmcos.Main")
  )
  .enablePlugins(ScalaJSPlugin)
