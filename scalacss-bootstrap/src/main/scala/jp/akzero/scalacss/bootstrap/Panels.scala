package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Panels extends StyleSheet.Inline {

  import dsl._

  val panel = style(addClassName("panel"))
  val panel_body = style(addClassName("panel-body"))
  val panel_collapse = style(addClassName("panel-collapse"))
  val panel_danger = style(addClassName("panel-danger"))
  val panel_default = style(addClassName("panel-default"))
  val panel_footer = style(addClassName("panel-footer"))
  val panel_group = style(addClassName("panel-group"))
  val panel_heading = style(addClassName("panel-heading"))
  val panel_info = style(addClassName("panel-info"))
  val panel_primary = style(addClassName("panel-primary"))
  val panel_success = style(addClassName("panel-success"))
  val panel_title = style(addClassName("panel-title"))
  val panel_warning = style(addClassName("panel-warning"))
}
