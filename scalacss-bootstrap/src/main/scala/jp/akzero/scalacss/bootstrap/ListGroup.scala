package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object ListGroup extends StyleSheet.Inline {

  import dsl._

  val list_group = style(addClassName("list-group"))
  val list_group_item = style(addClassName("list-group-item"))
  val list_group_item_danger = style(addClassName("list-group-item-danger"))
  val list_group_item_heading = style(addClassName("list-group-item-heading"))
  val list_group_item_info = style(addClassName("list-group-item-info"))
  val list_group_item_success = style(addClassName("list-group-item-success"))
  val list_group_item_text = style(addClassName("list-group-item-text"))
  val list_group_item_warning = style(addClassName("list-group-item-warning"))
}
