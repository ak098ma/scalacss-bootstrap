package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Forms extends StyleSheet.Inline {

  import dsl._

  val form_control = style(addClassName("form-control"))
  val form_control_feedback = style(addClassName("form-control-feedback"))
  val form_control_static = style(addClassName("form-control-static"))
  val form_group = style(addClassName("form-group"))
  val form_horizontal = style(addClassName("form-horizontal"))
  val form_inline = style(addClassName("form-inline"))

  val sr_only = Helpers.sr_only

  val input_group = style(addClassName("input-group"))
  val input_group_addon = style(addClassName("input-group-addon"))

  val disabled = style(addClassName("disabled"))
  val radio = style(addClassName("radio"))
  val radio_inline = style(addClassName("radio-inline"))
  val checkbox = style(addClassName("checkbox"))
  val checkbox_inline = style(addClassName("checkbox-inline"))

  val has_error = style(addClassName("has-error"))
  val has_success = style(addClassName("has-success"))
  val has_warning = style(addClassName("has-warning"))
  val has_feedback = style(addClassName("has-feedback"))

  val input_lg = style(addClassName("input-lg"))
  val input_sm = style(addClassName("input-sm"))

  val input_group_lg = style(addClassName("input-group-lg"))
  val input_group_sm = style(addClassName("input-group-sm"))

  val input_group_btn = style(addClassName("input-group-btn"))
}
