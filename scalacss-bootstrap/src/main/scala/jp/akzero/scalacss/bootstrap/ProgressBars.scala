package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object ProgressBars extends StyleSheet.Inline {

  import dsl._

  val progress = style(addClassName("progress"))
  val progress_bar = style(addClassName("progress-bar"))
  val progress_bar_danger = style(addClassName("progress-bar-danger"))
  val progress_bar_info = style(addClassName("progress-bar-info"))
  val progress_bar_success = style(addClassName("progress-bar-success"))
  val progress_bar_warning = style(addClassName("progress-bar-warning"))
  val progress_striped = style(addClassName("progress-striped"))

  val active = Helpers.active
}
