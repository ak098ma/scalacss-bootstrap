package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Buttons extends StyleSheet.Inline {

  import dsl._

  val active = Helpers.active

  val btn = style(addClassName("btn"))

  val btn_block = style(addClassName("btn-block"))

  val btn_primary = style(addClassName("btn-primary"))
  val btn_default = style(addClassName("btn-default"))
  val btn_danger = style(addClassName("btn-danger"))
  val btn_info = style(addClassName("btn-info"))
  val btn_success = style(addClassName("btn-success"))
  val btn_warning = style(addClassName("btn-warning"))

  val btn_group = style(addClassName("btn-group"))
  val btn_group_lg = style(addClassName("btn-group-lg"))
  val btn_group_sm = style(addClassName("btn-group-sm"))
  val btn_group_xs = style(addClassName("btn-group-xs"))
  val btn_group_justified = style(addClassName("btn-group-justified"))
  val btn_group_vertical = style(addClassName("btn-group-vertical"))

  val btn_xs = style(addClassName("btn-xs"))
  val btn_sm = style(addClassName("btn-sm"))
  val btn_lg = style(addClassName("btn-lg"))

  val btn_link = style(addClassName("btn-link"))
  val btn_toolbar = style(addClassName("btn-toolbar"))
}
