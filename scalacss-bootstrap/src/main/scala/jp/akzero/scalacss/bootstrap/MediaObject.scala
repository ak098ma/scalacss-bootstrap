package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object MediaObject extends StyleSheet.Inline {

  import dsl._

  val media = style(addClassName("media"))
  val media_body = style(addClassName("media-body"))
  val media_heading = style(addClassName("media-heading"))
  val media_list = style(addClassName("media-list"))
  val media_object = style(addClassName("media-object"))
}
