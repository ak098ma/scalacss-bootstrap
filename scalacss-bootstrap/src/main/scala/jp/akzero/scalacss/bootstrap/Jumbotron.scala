package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Jumbotron extends StyleSheet.Inline {
  import dsl._

  val jumbotron = style(addClassName("jumbotron"))
}
