package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Pagination extends StyleSheet.Inline {
  import dsl._

  val pagination = style(addClassName("pagination"))
  val pagination_lg = style(addClassName("pagination-lg"))
  val pagination_sm = style(addClassName("pagination-sm"))
}
