package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Navbar extends StyleSheet.Inline {

  import dsl._

  val navbar = style(addClassName("navbar"))
  val navbar_brand = style(addClassName("navbar-brand"))
  val navbar_btn = style(addClassName("navbar-btn"))
  val navbar_collapse = style(addClassName("navbar-collapse"))
  val navbar_default = style(addClassName("navbar-default"))
  val navbar_fixed_bottom = style(addClassName("navbar-fixed-bottom"))
  val navbar_fixed_top = style(addClassName("navbar-fixed-top"))
  val navbar_form = style(addClassName("navbar-form"))
  val navbar_header = style(addClassName("navbar-header"))
  val navbar_inverse = style(addClassName("navbar-inverse"))
  val navbar_left = style(addClassName("navbar-left"))
  val navbar_link = style(addClassName("navbar-link"))
  val navbar_nav = style(addClassName("navbar-nav"))
  val navbar_right = style(addClassName("navbar-right"))
  val navbar_static_top = style(addClassName("navbar-static-top"))
  val navbar_text = style(addClassName("navbar-text"))
  val navbar_toggle = style(addClassName("navbar-toggle"))
}
