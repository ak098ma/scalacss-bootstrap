package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Code extends StyleSheet.Inline {
  import dsl._

  val pre_scrollable = style(addClassName("pre-scrollable"))
}
