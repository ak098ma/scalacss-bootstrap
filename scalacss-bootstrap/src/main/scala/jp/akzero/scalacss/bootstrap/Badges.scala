package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Badges extends StyleSheet.Inline {
  import dsl._

  val badge = style(addClassName("badge"))
}
