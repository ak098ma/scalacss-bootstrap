package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Thumbnails extends StyleSheet.Inline {

  import dsl._

  val thumbnail = style(addClassName("thumbnail"))
  val caption = style(addClassName("caption"))
}
