package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Wells extends StyleSheet.Inline {

  import dsl._

  val well = style(addClassName("well"))
  val well_lg = style(addClassName("well-lg"))
  val well_sm = style(addClassName("well-sm"))
}
