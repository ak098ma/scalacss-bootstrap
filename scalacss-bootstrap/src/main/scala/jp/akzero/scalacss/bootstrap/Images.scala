package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Images extends StyleSheet.Inline {

  import dsl._

  val img_responsive = style(addClassName("img-responsive"))
  val img_rounded = style(addClassName("img-rounded"))
  val img_circle = style(addClassName("img-circle"))
  val img_thumbnail = style(addClassName("img-thumbnail"))
}
