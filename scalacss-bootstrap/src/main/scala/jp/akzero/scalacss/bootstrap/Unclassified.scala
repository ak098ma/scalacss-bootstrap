package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  * bootstrap_3_full_class_list: https://gist.github.com/mrcybermac/9175466
  * simply converted and not classified yet.
  */
object Unclassified extends StyleSheet.Inline {

  import dsl._

  val affix = style(addClassName("affix"))
  val arrow = style(addClassName("arrow"))
  val bottom = style(addClassName("bottom"))
  val bottom_left = style(addClassName("bottom-left"))
  val bottom_right = style(addClassName("bottom-right"))
  val breadcrumb = style(addClassName("breadcrumb"))
  val caret = style(addClassName("caret"))
  val carousel = style(addClassName("carousel"))
  val carousel_caption = style(addClassName("carousel-caption"))
  val carousel_control = style(addClassName("carousel-control"))
  val carousel_indicators = style(addClassName("carousel-indicators"))
  val carousel_inner = style(addClassName("carousel-inner"))
  val center_block = style(addClassName("center-block"))
  val collapse = style(addClassName("collapse"))
  val collapsing = style(addClassName("collapsing"))
  val control_label = style(addClassName("control-label"))
  val divider = style(addClassName("divider"))
  val fade = style(addClassName("fade"))
  val help_block = style(addClassName("help-block"))
  val icon_bar = style(addClassName("icon-bar"))
  val icon_next = style(addClassName("icon-next"))
  val icon_prev = style(addClassName("icon-prev"))
  val in = style(addClassName("in"))
  val item = style(addClassName("item"))
  val left = style(addClassName("left"))
  val modal = style(addClassName("modal"))
  val modal_backdrop = style(addClassName("modal-backdrop"))
  val modal_body = style(addClassName("modal-body"))
  val modal_content = style(addClassName("modal-content"))
  val modal_dialog = style(addClassName("modal-dialog"))
  val modal_footer = style(addClassName("modal-footer"))
  val modal_header = style(addClassName("modal-header"))
  val modal_lg = style(addClassName("modal-lg"))
  val modal_open = style(addClassName("modal-open"))
  val modal_sm = style(addClassName("modal-sm"))
  val modal_title = style(addClassName("modal-title"))
  val next = style(addClassName("next"))
  val open = style(addClassName("open"))
  val pager = style(addClassName("pager"))
  val popover = style(addClassName("popover"))
  val popover_content = style(addClassName("popover-content"))
  val popover_title = style(addClassName("popover-title"))
  val prev = style(addClassName("prev"))
  val previous = style(addClassName("previous"))
  val right = style(addClassName("right"))
  val tab_content = style(addClassName("tab-content"))
  val tab_pane = style(addClassName("tab-pane"))
  val tooltip = style(addClassName("tooltip"))
  val tooltip_arrow = style(addClassName("tooltip-arrow"))
  val tooltip_inner = style(addClassName("tooltip-inner"))
  val top = style(addClassName("top"))
  val top_left = style(addClassName("top-left"))
  val top_right = style(addClassName("top-right"))
}
