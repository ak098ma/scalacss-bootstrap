package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Tables extends StyleSheet.Inline {

  import dsl._

  val table = style(addClassName("table"))
  val table_bordered = style(addClassName("table-bordered"))
  val table_condensed = style(addClassName("table-condensed"))
  val table_hover = style(addClassName("table-hover"))
  val table_responsive = style(addClassName("table-responsive"))
  val table_striped = style(addClassName("table-striped"))

  val active = style(addClassName("active"))
  val success = style(addClassName("success"))
  val info = style(addClassName("info"))
  val warning = style(addClassName("warning"))
  val danger = style(addClassName("danger"))

  val sr_only = Helpers.sr_only
}
