package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Alerts extends StyleSheet.Inline {

  import dsl._

  val alert = style(addClassName("alert"))
  val alert_danger = style(addClassName("alert-danger"))
  val alert_dismissable = style(addClassName("alert-dismissable"))
  val alert_info = style(addClassName("alert-info"))
  val alert_link = style(addClassName("alert-link"))
  val alert_success = style(addClassName("alert-success"))
  val alert_warning = style(addClassName("alert-warning"))
}
