package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Navs extends StyleSheet.Inline {

  import dsl._

  val active = Helpers.active

  val nav = style(addClassName("nav"))
  val nav_divider = style(addClassName("nav-divider"))
  val nav_justified = style(addClassName("nav-justified"))
  val nav_pills = style(addClassName("nav-pills"))
  val nav_stacked = style(addClassName("nav-stacked"))
  val nav_tabs = style(addClassName("nav-tabs"))
  val nav_tabs_justified = style(addClassName("nav-tabs-justified"))
}
