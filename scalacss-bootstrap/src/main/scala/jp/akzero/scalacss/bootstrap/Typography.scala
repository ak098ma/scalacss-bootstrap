package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Typography extends StyleSheet.Inline {

  import dsl._

  val h1 = style(addClassName("h1"))
  val h2 = style(addClassName("h2"))
  val h3 = style(addClassName("h3"))
  val h4 = style(addClassName("h4"))
  val h5 = style(addClassName("h5"))
  val h6 = style(addClassName("h6"))

  val lead = style(addClassName("lead"))

  val small = style(addClassName("small"))

  val text_danger = Helpers.text_danger
  val text_info = Helpers.text_info
  val text_muted = Helpers.text_muted
  val text_primary = Helpers.text_primary
  val text_success = Helpers.text_success
  val text_warning = Helpers.text_warning

  val text_center = style(addClassName("text-center"))
  val text_hide = Helpers.text_hide
  val text_justify = style(addClassName("text-justify"))
  val text_left = style(addClassName("text-left"))
  val text_right = style(addClassName("text-right"))

  val text_lowercase = style(addClassName("text-lowercase"))
  val text_uppercase = style(addClassName("text-uppercase"))
  val text_capitalize = style(addClassName("text-capitalize"))

  val initialism = style(addClassName("initialism"))

  val blockquote_reverse = style(addClassName("blockquote-reverse"))

  val list_inline = style(addClassName("list-inline"))
  val list_unstyled = style(addClassName("list-unstyled"))

  val dl_horizontal = style(addClassName("dl-horizontal"))
}
