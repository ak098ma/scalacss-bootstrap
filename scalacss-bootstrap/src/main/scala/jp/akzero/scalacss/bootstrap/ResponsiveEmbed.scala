package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object ResponsiveEmbed extends StyleSheet.Inline {

  import dsl._

  val embed_responsive = style(addClassName("embed-responsive"))
  val embed_responsive_16by9 = style(addClassName("embed-responsive-16by9"))
  val embed_responsive_4by3 = style(addClassName("embed-responsive-4by3"))
}
