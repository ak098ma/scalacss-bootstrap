package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Dropdowns extends StyleSheet.Inline {

  import dsl._

  val dropdown = style(addClassName("dropdown"))
  val dropdown_backdrop = style(addClassName("dropdown-backdrop"))
  val dropdown_header = style(addClassName("dropdown-header"))
  val dropdown_menu = style(addClassName("dropdown-menu"))
  val dropdown_menu_left = style(addClassName("dropdown-menu-left"))
  val dropdown_menu_right = style(addClassName("dropdown-menu-right"))
  val dropdown_toggle = style(addClassName("dropdown-toggle"))
  val dropup = style(addClassName("dropup"))
}
