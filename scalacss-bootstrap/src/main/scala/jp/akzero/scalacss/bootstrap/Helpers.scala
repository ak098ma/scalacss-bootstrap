package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Helpers extends StyleSheet.Inline {

  import dsl._

  val active = style(addClassName("active"))
  val disabled = style(addClassName("disabled"))

  val sr_only = style(addClassName("sr-only"))
  val sr_only_focusable = style(addClassName("sr-only-focusable"))

  val text_danger = style(addClassName("text-danger"))
  val text_info = style(addClassName("text-info"))
  val text_muted = style(addClassName("text-muted"))
  val text_primary = style(addClassName("text-primary"))
  val text_success = style(addClassName("text-success"))
  val text_warning = style(addClassName("text-warning"))

  val text_hide = style(addClassName("text-hide"))

  val bg_danger = style(addClassName("bg-danger"))
  val bg_info = style(addClassName("bg-info"))
  val bg_primary = style(addClassName("bg-primary"))
  val bg_success = style(addClassName("bg-success"))
  val bg_warning = style(addClassName("bg-warning"))

  val close = style(addClassName("close"))

  val container = style(addClassName("container"))
  val container_fluid = style(addClassName("container-fluid"))

  val pull_left = style(addClassName("pull-left"))
  val pull_right = style(addClassName("pull-right"))

  val clearfix = style(addClassName("clearfix"))

  val show = style(addClassName("show"))
  val hide = style(addClassName("hide"))
  val hidden = style(addClassName("hidden"))

  val invisible = style(addClassName("invisible"))

  val visible_xs_1 = style(addClassName("visible-xs-1"))
  val visible_xs_2 = style(addClassName("visible-xs-2"))
  val visible_xs_3 = style(addClassName("visible-xs-3"))
  val visible_xs_4 = style(addClassName("visible-xs-4"))
  val visible_xs_5 = style(addClassName("visible-xs-5"))
  val visible_xs_6 = style(addClassName("visible-xs-6"))
  val visible_xs_7 = style(addClassName("visible-xs-7"))
  val visible_xs_8 = style(addClassName("visible-xs-8"))
  val visible_xs_9 = style(addClassName("visible-xs-9"))
  val visible_xs_10 = style(addClassName("visible-xs-10"))
  val visible_xs_11 = style(addClassName("visible-xs-11"))
  val visible_xs_12 = style(addClassName("visible-xs-12"))
  val visible_xs_block = style(addClassName("visible-xs-block"))
  val visible_xs_inline = style(addClassName("visible-xs-inline"))
  val visible_xs_inline_block = style(addClassName("visible-xs-inline-block"))

  val visible_sm_1 = style(addClassName("visible-sm-1"))
  val visible_sm_2 = style(addClassName("visible-sm-2"))
  val visible_sm_3 = style(addClassName("visible-sm-3"))
  val visible_sm_4 = style(addClassName("visible-sm-4"))
  val visible_sm_5 = style(addClassName("visible-sm-5"))
  val visible_sm_6 = style(addClassName("visible-sm-6"))
  val visible_sm_7 = style(addClassName("visible-sm-7"))
  val visible_sm_8 = style(addClassName("visible-sm-8"))
  val visible_sm_9 = style(addClassName("visible-sm-9"))
  val visible_sm_10 = style(addClassName("visible-sm-10"))
  val visible_sm_11 = style(addClassName("visible-sm-11"))
  val visible_sm_12 = style(addClassName("visible-sm-12"))
  val visible_sm_block = style(addClassName("visible-sm-block"))
  val visible_sm_inline = style(addClassName("visible-sm-inline"))
  val visible_sm_inline_block = style(addClassName("visible-sm-inline-block"))

  val visible_md_1 = style(addClassName("visible-md-1"))
  val visible_md_2 = style(addClassName("visible-md-2"))
  val visible_md_3 = style(addClassName("visible-md-3"))
  val visible_md_4 = style(addClassName("visible-md-4"))
  val visible_md_5 = style(addClassName("visible-md-5"))
  val visible_md_6 = style(addClassName("visible-md-6"))
  val visible_md_7 = style(addClassName("visible-md-7"))
  val visible_md_8 = style(addClassName("visible-md-8"))
  val visible_md_9 = style(addClassName("visible-md-9"))
  val visible_md_10 = style(addClassName("visible-md-10"))
  val visible_md_11 = style(addClassName("visible-md-11"))
  val visible_md_12 = style(addClassName("visible-md-12"))
  val visible_md_block = style(addClassName("visible-md-block"))
  val visible_md_inline = style(addClassName("visible-md-inline"))
  val visible_md_inline_block = style(addClassName("visible-md-inline-block"))

  val visible_lg_1 = style(addClassName("visible-lg-1"))
  val visible_lg_2 = style(addClassName("visible-lg-2"))
  val visible_lg_3 = style(addClassName("visible-lg-3"))
  val visible_lg_4 = style(addClassName("visible-lg-4"))
  val visible_lg_5 = style(addClassName("visible-lg-5"))
  val visible_lg_6 = style(addClassName("visible-lg-6"))
  val visible_lg_7 = style(addClassName("visible-lg-7"))
  val visible_lg_8 = style(addClassName("visible-lg-8"))
  val visible_lg_9 = style(addClassName("visible-lg-9"))
  val visible_lg_10 = style(addClassName("visible-lg-10"))
  val visible_lg_11 = style(addClassName("visible-lg-11"))
  val visible_lg_12 = style(addClassName("visible-lg-12"))
  val visible_lg_block = style(addClassName("visible-lg-block"))
  val visible_lg_inline = style(addClassName("visible-lg-inline"))
  val visible_lg_inline_block = style(addClassName("visible-lg-inline-block"))

  val visible_print_block = style(addClassName("visible-print-block"))
  val visible_print_inline = style(addClassName("visible-print-inline"))
  val visible_print_inline_block = style(addClassName("visible-print-inline-block"))

  val hidden_xs = style(addClassName("hidden-xs"))
  val hidden_sm = style(addClassName("hidden-sm"))
  val hidden_md = style(addClassName("hidden-md"))
  val hidden_lg = style(addClassName("hidden-lg"))
  val hidden_print = style(addClassName("hidden-print"))
}
