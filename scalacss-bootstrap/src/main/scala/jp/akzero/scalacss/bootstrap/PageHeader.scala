package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object PageHeader extends StyleSheet.Inline {

  import dsl._

  val page_header = style(addClassName("page-header"))
}
