package jp.akzero.scalacss.bootstrap

import scalacss.Defaults._

/**
  * ScalaCSS bindings for Bootstrap.
  */
object Labels extends StyleSheet.Inline {
  import dsl._

  val label = style(addClassName("label"))
  val label_danger = style(addClassName("label-danger"))
  val label_default = style(addClassName("label-default"))
  val label_info = style(addClassName("label-info"))
  val label_primary = style(addClassName("label-primary"))
  val label_success = style(addClassName("label-success"))
  val label_warning = style(addClassName("label-warning"))
}
